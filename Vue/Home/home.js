import Vue from 'vue'
import Home from './Home.vue'

Vue.config.productionTip = false

// start app
new Vue({
    components: {
        Home,
    },
    render: h => h(Home),
}).$mount('#app-home')
