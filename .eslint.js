module.exports = {
    root: true,
    env: {
        node: true,
    },
    plugins: [
        'vue',
    ],
    extends: [
//         'eslint:recommended',
        'plugin:vue/recommended',
    ],
    globals: {
        window: true,
        location: true,
        document: true,
        module: true,
        __dirname: true,
    },
    rules: {
        'no-console': 'off',
        'no-debugger': 'off',
        'vue/require-default-prop': 'off',
        'vue/html-indent': 'off',
        'max-len': [
            1,
            {
                code: 120,
                ignoreUrls: true,
            },
        ],
        quotes: [
            'error',
            'single',
            {
                avoidEscape: true,
            },
        ],
        'vue/html-self-closing': [
            'error',
            {
                html: {
                    void: 'any',
                },
            },
        ],
        'vue/max-attributes-per-line': [
            2,
            {
                singleline: 120,
                multiline: {
                    max: 2,
                    allowFirstLine: true,
                },
            },
        ],
    },
    parserOptions: {
        parser: 'babel-eslint',
        ecmaVersion: 2018,
        sourceType: 'module',
        ecmaFeatures: {
            jsx: true,
        },
    },
    env: {
        amd: true,
        browser: true,
        es6: true,
    },
}
