var path = require('path')
var ManifestPlugin = require('webpack-manifest-plugin')
// var HtmlPlugin = require('html-webpack-plugin')
var { StatsWriterPlugin } = require('webpack-stats-plugin')


const outputDir = 'Content/Built'
const VueDir = path.resolve(__dirname, 'Vue/')
let VueCliConfig = {
    pages: {
        home: {
            // entry for the page
            entry: 'Vue/Home/home.js',
            chunks: ['chunk-vendors', 'chunk-common', 'home-js'],
            template: 'Views/Shared/Webpack/_js.ejs',
            filename: 'templates/home/js.html',
            // Webpack aditional options
            hash: true,
            inject: false,
            minify: {
                collapseWhitespace: true
            },
        },
        'home-css': {
            // entry for the page
            entry: 'Vue/Home/home.scss',
            chunks: ['css', 'home-css', 'home'],
            template: 'Views/Shared/Webpack/_css.ejs',
            filename: 'templates/home/css.html',
            // Webpack aditional options
            hash: true,
            inject: false,
            minify: {
                collapseWhitespace: true
            },
        },
    },
    outputDir: outputDir,
    productionSourceMap: true,
    filenameHashing: true,
    configureWebpack: {
        // stats: 'verbose',
        resolve: {
            alias: {
                '~': VueDir,
                '@': VueDir,
            }
        },
        plugins: [
            new ManifestPlugin(),
            new StatsWriterPlugin({
                all: true,
                filename: 'stats.json',
                fields: null,
            }),
        ],
    },
};

module.exports = VueCliConfig
